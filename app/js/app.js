'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('LearnApp', [
  'ngRoute',
  'LearnApp.RestService',
  'LearnApp.topics',
  'LearnApp.posts'
]);

app.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider.when('/topics', {
      templateUrl: '/app/js/topics/partials/topics-list.html',
      controller: "LearnApp.topics.listCtrl"
    }).
    when('/topics/:id', {
      templateUrl: '/app/js/topics/partials/topic.html',
      controller: "LearnApp.topics.topicCtrl"
    }).
    when('/404',{
      templateUrl: "/app/js/components/partials/404.html"
    }).
    otherwise({
      redirectTo: '/topics'
    })
  }
]);
