/**
 * Created by daniel on 2016-01-27.
 */
var topics = angular.module('LearnApp.topics', []);

topics.controller('LearnApp.topics.listCtrl', ['$scope', 'LearnApp.RestService.Topics',
  function($scope, restTopics){
    $scope.topicsCount = 0;
    $scope.topics = [];

    restTopics.doTopicsList().$promise.then(function (response) {
      if (response.$resolved) {
        $scope.topics = response.data;
        $scope.topicsCount = $scope.topics.length
      } else{
        console.log('error');
      }
    });

    $scope.orderTitle = '';
    $scope.orderCreated = '';

    $scope.doOrderTopics = function () {
      var order = [[],[]];
      if ($scope.orderTitle) {
        order[0].push('title');
        order[1].push($scope.orderTitle);
      }
      if ($scope.orderCreated) {
        order[0].push('created');
        order[1].push($scope.orderCreated);
      }
      $scope.topics = _.orderBy($scope.topics, order[0], order[1]);
    }
  }
]);

topics.controller('LearnApp.topics.topicCtrl', ['$scope', '$routeParams', '$location',
  'LearnApp.RestService.Topics',
  function ($scope,  $routeParams, $location, restTopics) {

    $scope.topic = {
      title: "Test title",
      author: {
        username: "Test author"
      }
    };

    restTopics.doTopic($routeParams).$promise.then(function(response) {
      if (response.$resolved) {
        $scope.topic = response.data;
      } else {
        console.log('response not resolved', response);
      }
    }, function (response){
      console.log('error', response);
      $location.path('/404')
    });

  }
]);
