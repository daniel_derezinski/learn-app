/**
 * Created by daniel on 2016-01-27.
 */
var rest = angular.module("LearnApp.RestService", ['ngResource']);

rest.factory('LearnApp.RestService.Topics', ['$resource', function ($resource) {
  return $resource('http://localhost:3000/topics/:id', {}, {
    doTopicsList: {method: "GET", params: {}, transformResponse: [transformResponse, topicsDate]},
    doTopic: {method: "GET", params: {}, transformResponse: [transformResponse, topicDate]}
  })
}]);

var transformResponse = function (data, headers) {
  var response = {};
  if (data) {
    response.data = angular.fromJson(data);
  }
  response.headers = headers();
  return response
};

var topicsDate = function(response) {
  if (response.data) {
    response.data = _.map(response.data, function (ele)  {
      ele.created = new Date(ele.created);
      return ele
    });
  }
  return response
};

var topicDate = function(response) {
  if (response.data) {
    response.data.created = new Date(response.data.created);
  }
  return response;
};

rest.factory('LearnApp.RestService.Posts', ['$resource', function ($resource) {
  return $resource('http://localhost:3000/posts/:id', {}, {
    doTopicPosts: {method: "GET", url: "http://localhost:3000/posts/topic/:id", params: {}, transformResponse: [transformResponse, topicsDate]},
    createPost: {method: "POST", url: "http://localhost:3000/posts" },
    deletePost: {method: "DELETE"},
    editPost: {method: "PUT"}
  })
}]);

rest.factory("LearnApp.RestService.Authors", ["$resource", function ($resource) {
  return $resource('http://localhost:3000/authors', {}, {
    doAuthorsList: {method: "GET", params: {}, isArray:true}
  })
}]);
